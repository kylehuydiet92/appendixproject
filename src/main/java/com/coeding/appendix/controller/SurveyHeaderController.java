package com.coeding.appendix.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coeding.appendix.entity.Organizations;
import com.coeding.appendix.entity.Survey_Headers;
import com.coeding.appendix.service.OrganizationService;
import com.coeding.appendix.service.SurveyHeaderService;

@RestController
@RequestMapping("/api/v1/survey-header")
public class SurveyHeaderController {
	@Autowired
	private SurveyHeaderService service;
	@GetMapping(value = {"","/"})
	public ResponseEntity<List<Survey_Headers>> getAll(){
		return new ResponseEntity<List<Survey_Headers>>(service.list(null),HttpStatus.OK);
	}
	
	@GetMapping(value = {"/{id}"})
	public ResponseEntity<Survey_Headers> get(Survey_Headers survey_Headers){
		return new ResponseEntity<Survey_Headers>(service.get(survey_Headers),HttpStatus.OK);
	}
	@PostMapping("")
	public ResponseEntity<Boolean> insert(@RequestBody Survey_Headers survey_Headers){
		System.out.println(survey_Headers);
		if(survey_Headers !=null) {
			service.insert(survey_Headers);
			return new ResponseEntity<Boolean>(true,HttpStatus.OK);
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND);
	}
	@PutMapping("/{id}")
	public ResponseEntity<Boolean> update(@PathVariable int id,@RequestBody Survey_Headers survey_Headers){
		if(survey_Headers !=null) {
			survey_Headers.setId(id);
			Survey_Headers getOne = service.get(survey_Headers);
			if(getOne != null) {
				service.update(survey_Headers);
				return new ResponseEntity<Boolean>(true,HttpStatus.OK); 
			}else {
				return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND);
			}
			
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NO_CONTENT);
		
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delele(Survey_Headers survey_Headers){
		Survey_Headers getOne =service.get(survey_Headers);
		if(getOne != null) {
			service.delete(getOne);
			return new ResponseEntity<Boolean>(true,HttpStatus.OK); 
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND); 
	}
}
