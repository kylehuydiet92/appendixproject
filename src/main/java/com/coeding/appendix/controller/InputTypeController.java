package com.coeding.appendix.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coeding.appendix.entity.Input_Types;

import com.coeding.appendix.service.InputTypeService;


@RestController
@RequestMapping("/api/v1/inputtype")
public class InputTypeController {
	@Autowired
	private InputTypeService service;
	@GetMapping(value = {"","/"})
	public ResponseEntity<List<Input_Types>> getAll(){
		return new ResponseEntity<List<Input_Types>>(service.list(null),HttpStatus.OK);
	}
	
	@GetMapping(value = {"/{id}"})
	public ResponseEntity<Input_Types> get(Input_Types input_Types){
		return new ResponseEntity<Input_Types>(service.get(input_Types),HttpStatus.OK);
	}
	@PostMapping("")
	public ResponseEntity<Boolean> insert(@RequestBody Input_Types input_Types){
		if(input_Types !=null) {
			service.insert(input_Types);
			return new ResponseEntity<Boolean>(true,HttpStatus.OK);
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND);
	}
	@PutMapping("/{id}")
	public ResponseEntity<Boolean> update(@PathVariable int id,@RequestBody Input_Types input_Types){
		if(input_Types !=null) {
			input_Types.setId(id);
			Input_Types getOne = service.get(input_Types);
			if(getOne != null) {
				service.update(input_Types);
				return new ResponseEntity<Boolean>(true,HttpStatus.OK); 
			}else {
				return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND);
			}
			
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NO_CONTENT);
		
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delele(Input_Types input_Types){
		Input_Types getOne =service.get(input_Types);
		if(getOne != null) {
			service.delete(getOne);
			return new ResponseEntity<Boolean>(true,HttpStatus.OK); 
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND); 
	}
}
