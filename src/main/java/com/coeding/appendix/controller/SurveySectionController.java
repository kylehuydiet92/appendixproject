package com.coeding.appendix.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coeding.appendix.entity.Organizations;
import com.coeding.appendix.entity.SurveySection;
import com.coeding.appendix.service.OrganizationService;
import com.coeding.appendix.service.SurveySectionService;

@RestController
@RequestMapping("/api/v1/survey-section")
public class SurveySectionController {
	@Autowired
	private SurveySectionService service;
	@GetMapping(value = {"","/"})
	public ResponseEntity<List<SurveySection>> getAll(){
		return new ResponseEntity<List<SurveySection>>(service.list(null),HttpStatus.OK);
	}
	
	@GetMapping(value = {"/{id}"})
	public ResponseEntity<SurveySection> get(SurveySection surveySection){
		return new ResponseEntity<SurveySection>(service.get(surveySection),HttpStatus.OK);
	}
	@PostMapping("")
	public ResponseEntity<Boolean> insert(@RequestBody SurveySection surveySection){
		if(surveySection !=null) {
			service.insert(surveySection);
			return new ResponseEntity<Boolean>(true,HttpStatus.OK);
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND);
	}
	@PutMapping("/{id}")
	public ResponseEntity<Boolean> update(@PathVariable int id,@RequestBody SurveySection surveySection){
		if(surveySection !=null) {
			surveySection.setId(id);
			SurveySection getOne = service.get(surveySection);
			if(getOne != null) {
				service.update(surveySection);
				return new ResponseEntity<Boolean>(true,HttpStatus.OK); 
			}else {
				return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND);
			}
			
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NO_CONTENT);
		
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delele(SurveySection surveySection){
		SurveySection getOne =service.get(surveySection);
		if(getOne != null) {
			service.delete(getOne);
			return new ResponseEntity<Boolean>(true,HttpStatus.OK); 
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND); 
	}
}
