package com.coeding.appendix.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coeding.appendix.entity.OptionGroup;
import com.coeding.appendix.service.OptionGroupService;

@RestController
@RequestMapping("/api/v1/option-group")
public class OptionGroupController {
	@Autowired
	private OptionGroupService service;
	@GetMapping(value = {"","/"})
	public ResponseEntity<List<OptionGroup>> getAll(){
		return new ResponseEntity<List<OptionGroup>>(service.list(null),HttpStatus.OK);
	}
	
	@GetMapping(value = {"/{id}"})
	public ResponseEntity<OptionGroup> get(OptionGroup optionGroup){
		return new ResponseEntity<OptionGroup>(service.get(optionGroup),HttpStatus.OK);
	}
	@PostMapping("")
	public ResponseEntity<Boolean> insert(@RequestBody OptionGroup optionGroup){
		if(optionGroup !=null) {
			service.insert(optionGroup);
			return new ResponseEntity<Boolean>(true,HttpStatus.OK);
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND);
	}
	@PutMapping("/{id}")
	public ResponseEntity<Boolean> update(@PathVariable int id,@RequestBody OptionGroup optionGroup){
		if(optionGroup !=null) {
			optionGroup.setId(id);
			OptionGroup getOne = service.get(optionGroup);
			if(getOne != null) {
				service.update(optionGroup);
				return new ResponseEntity<Boolean>(true,HttpStatus.OK); 
			}else {
				return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND);
			}
			
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NO_CONTENT);
		
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delele(OptionGroup optionGroup){
		OptionGroup getOne =service.get(optionGroup);
		if(getOne != null) {
			service.delete(getOne);
			return new ResponseEntity<Boolean>(true,HttpStatus.OK); 
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND); 
	}
}
