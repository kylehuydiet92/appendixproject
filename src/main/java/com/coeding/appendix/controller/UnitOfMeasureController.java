package com.coeding.appendix.controller;

import java.awt.PageAttributes.MediaType;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coeding.appendix.entity.Unit_Of_Measures;
import com.coeding.appendix.service.MearuseService;

@RestController
@RequestMapping("/api/v1/measure")
public class UnitOfMeasureController {
	@Autowired
	private MearuseService mearuseService;

	@GetMapping(value = "",produces="application/json")
	public ResponseEntity<List<Unit_Of_Measures>>  getAllMeasure() {

		if (mearuseService != null) {
			List<Unit_Of_Measures> list = mearuseService.list(null);
			return  new ResponseEntity<List<Unit_Of_Measures>>(list,HttpStatus.OK);
		}
		return null;
	}

	@PostMapping(value = "",consumes = "application/json; charset=utf-8")
	public ResponseEntity<Boolean> createMeasure(@RequestBody Unit_Of_Measures measures) {
//		System.out.println(unit_of_measures_name);
		System.out.println(measures);
//		Unit_Of_Measures test = new Unit_Of_Measures();
//		test.setUnit_of_measures_name(unit_of_measures_name);
		if (measures != null) {
			if (measures.getUnit_of_measures_name() != null && !measures.getUnit_of_measures_name().equals("")) {
				mearuseService.insert(measures);
				return new ResponseEntity<Boolean>(true,HttpStatus.OK);
			}
		}
		 return new ResponseEntity<Boolean>(false,HttpStatus.NO_CONTENT);

	}

	@GetMapping("/{id}")
	public ResponseEntity<Unit_Of_Measures> getMeasure(@PathVariable("id") int id,Unit_Of_Measures measures) {
		System.out.println(measures);
		Unit_Of_Measures getOne = mearuseService.get(measures);
	
		return new ResponseEntity<Unit_Of_Measures>(getOne,HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Boolean> deleteMeasure(Unit_Of_Measures measures) {
		System.out.println(measures);
		Unit_Of_Measures getOne = mearuseService.get(measures);
		if(getOne != null) {
			mearuseService.delete(getOne);
			return new ResponseEntity<Boolean>(true,HttpStatus.OK);
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NO_CONTENT);
	}
	@PutMapping(value = "/{id}",
			consumes = "application/json; charset=utf-8")
	public ResponseEntity<Boolean> updateMeasure(@PathVariable("id") int id,@RequestBody Unit_Of_Measures measures) {
		measures.setId(id);
		System.out.println(measures);
		
		Unit_Of_Measures getOne = mearuseService.get(measures);
		if(getOne != null) {
		
			mearuseService.update(measures);
			return new ResponseEntity<Boolean>(true,HttpStatus.OK);
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NO_CONTENT);
	}
}
