package com.coeding.appendix.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coeding.appendix.entity.OptionGroup;
import com.coeding.appendix.entity.User;
import com.coeding.appendix.service.OptionGroupService;
import com.coeding.appendix.service.UserService;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {
	@Autowired
	private UserService service;
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class,"invite_dt", new CustomDateEditor(dateFormat, false));
		binder.registerCustomEditor(Date.class,"last_login_dt", new CustomDateEditor(dateFormat, false));
	}
	@GetMapping(value = {"","/"})
	public ResponseEntity<List<User>> getAll(){
		return new ResponseEntity<List<User>>(service.list(null),HttpStatus.OK);
	}
	
	@GetMapping(value = {"/{id}"})
	public ResponseEntity<User> get(User user){
		return new ResponseEntity<User>(service.get(user),HttpStatus.OK);
	}
	@PostMapping("")
	public ResponseEntity<Boolean> insert(@RequestBody User user){
		if(user !=null) {
			service.insert(user);
			return new ResponseEntity<Boolean>(true,HttpStatus.OK);
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND);
	}
	@PutMapping("/{id}")
	public ResponseEntity<Boolean> update(@PathVariable int id,@RequestBody User user){
		if(user !=null) {
			user.setId(id);
			User getOne = service.get(user);
			if(getOne != null) {
				service.update(user);
				return new ResponseEntity<Boolean>(true,HttpStatus.OK); 
			}else {
				return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND);
			}
			
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NO_CONTENT);
		
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delele(User user){
		User getOne =service.get(user);
		if(getOne != null) {
			service.delete(getOne);
			return new ResponseEntity<Boolean>(true,HttpStatus.OK); 
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND); 
	}

}
