package com.coeding.appendix.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coeding.appendix.entity.Organizations;
import com.coeding.appendix.service.OrganizationService;
@RestController
@RequestMapping("/api/v1/organization")
public class OrganizationController {
	@Autowired
	private OrganizationService service;
	@GetMapping(value = {"","/"})
	public ResponseEntity<List<Organizations>> getAll(){
		return new ResponseEntity<List<Organizations>>(service.list(null),HttpStatus.OK);
	}
	
	@GetMapping(value = {"/{id}"})
	public ResponseEntity<Organizations> get(Organizations organizations){
		return new ResponseEntity<Organizations>(service.get(organizations),HttpStatus.OK);
	}
	@PostMapping("")
	public ResponseEntity<Boolean> insert(@RequestBody Organizations organizations){
		if(organizations !=null) {
			service.insert(organizations);
			return new ResponseEntity<Boolean>(true,HttpStatus.OK);
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND);
	}
	@PutMapping("/{id}")
	public ResponseEntity<Boolean> update(@PathVariable int id,@RequestBody Organizations organizations){
		if(organizations !=null) {
			organizations.setId(id);
			Organizations getOne = service.get(organizations);
			if(getOne != null) {
				service.update(organizations);
				return new ResponseEntity<Boolean>(true,HttpStatus.OK); 
			}else {
				return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND);
			}
			
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NO_CONTENT);
		
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delele(Organizations organizations){
		Organizations getOne =service.get(organizations);
		if(getOne != null) {
			service.delete(getOne);
			return new ResponseEntity<Boolean>(true,HttpStatus.OK); 
		}
		return new ResponseEntity<Boolean>(false,HttpStatus.NOT_FOUND); 
	}
}
