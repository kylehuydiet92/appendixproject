package com.coeding.appendix.repository.impl;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.coeding.appendix.entity.Input_Types;
import com.coeding.appendix.repository.InputTypeRepository;
@Repository
public class InputTypeRepositoryImpl implements InputTypeRepository {
	@Autowired
	private SqlSessionTemplate session;
	@Override
	public void insert(Input_Types vo) {
		// TODO Auto-generated method stub
		session.insert("InputType.insert",vo);
	}

	@Override
	public void update(Input_Types vo) {
		// TODO Auto-generated method stub
		session.update("InputType.update", vo);
	}

	@Override
	public void delete(Input_Types vo) {
		// TODO Auto-generated method stub
		session.delete("InputType.delete", vo);
	}

	@Override
	public Input_Types get(Input_Types vo) {
		// TODO Auto-generated method stub
		return session.selectOne("InputType.get",vo);
	}

	@Override
	public List<Input_Types> list(Input_Types vo) {
		// TODO Auto-generated method stub
		return session.selectList("InputType.getList", null);
	}

}
