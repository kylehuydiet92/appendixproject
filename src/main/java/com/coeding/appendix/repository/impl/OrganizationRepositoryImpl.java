package com.coeding.appendix.repository.impl;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.coeding.appendix.entity.Organizations;
import com.coeding.appendix.repository.OrganizationRepository;
@Repository
public class OrganizationRepositoryImpl implements OrganizationRepository{
	@Autowired
	private SqlSessionTemplate session;
	@Override
	public void insert(Organizations vo) {
		session.insert("Organization.insert", vo);
		
	}

	@Override
	public void update(Organizations vo) {
		session.update("Organization.update",vo);
		
	}

	@Override
	public void delete(Organizations vo) {
		session.delete("Organization.delete", vo);		
	}

	@Override
	public Organizations get(Organizations vo) {
		// TODO Auto-generated method stub
		return session.selectOne("Organization.get", vo);
	}

	@Override
	public List<Organizations> list(Organizations vo) {
		// TODO Auto-generated method stub
		return session.selectList("Organization.getList", null);
	}

}
