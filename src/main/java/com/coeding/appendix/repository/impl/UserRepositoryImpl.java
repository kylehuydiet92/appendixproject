package com.coeding.appendix.repository.impl;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.coeding.appendix.entity.User;
import com.coeding.appendix.repository.UserRepository;
@Repository
public class UserRepositoryImpl implements UserRepository {
	@Autowired
	private SqlSessionTemplate session;
	@Override
	public void insert(User vo) {
		// TODO Auto-generated method stub
		session.insert("User.insert", vo);
	}

	@Override
	public void update(User vo) {
		// TODO Auto-generated method stub
		session.update("User.update", vo);
	}

	@Override
	public void delete(User vo) {
		// TODO Auto-generated method stub
		session.delete("User.delete", vo);
	}

	@Override
	public User get(User vo) {
		// TODO Auto-generated method stub
		return session.selectOne("User.get", vo);
	}

	@Override
	public List<User> list(User vo) {
		// TODO Auto-generated method stub
		return session.selectList("User.getList", null);
	}

}
