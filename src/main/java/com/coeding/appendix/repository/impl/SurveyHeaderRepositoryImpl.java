package com.coeding.appendix.repository.impl;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.coeding.appendix.entity.Survey_Headers;
import com.coeding.appendix.repository.SurveyHeaderRepository;
@Repository
public class SurveyHeaderRepositoryImpl implements SurveyHeaderRepository {
	@Autowired
	private SqlSessionTemplate session;
	@Override
	public void insert(Survey_Headers vo) {
		// TODO Auto-generated method stub
		session.insert("Survey-Header.insert", vo);
	}

	@Override
	public void update(Survey_Headers vo) {
		// TODO Auto-generated method stub
		session.update("Survey-Header.update", vo);
	}

	@Override
	public void delete(Survey_Headers vo) {
		// TODO Auto-generated method stub
		session.delete("Survey-Header.delete", vo);
	}

	@Override
	public Survey_Headers get(Survey_Headers vo) {
		// TODO Auto-generated method stub
		return session.selectOne("Survey-Header.get",vo);
	}

	@Override
	public List<Survey_Headers> list(Survey_Headers vo) {
		// TODO Auto-generated method stub
		return session.selectList("Survey-Header.getList", null);
	}

}
