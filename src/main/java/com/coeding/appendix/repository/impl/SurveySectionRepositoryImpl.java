package com.coeding.appendix.repository.impl;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.coeding.appendix.entity.SurveySection;
import com.coeding.appendix.repository.SurveySectionRepository;
@Repository
public class SurveySectionRepositoryImpl implements SurveySectionRepository {
	@Autowired
	private SqlSessionTemplate session;
	@Override
	public void insert(SurveySection vo) {
		// TODO Auto-generated method stub
		session.insert("SurveySection.insert", vo);
	}

	@Override
	public void update(SurveySection vo) {
		// TODO Auto-generated method stub
		session.update("SurveySection.update", vo);
	}

	@Override
	public void delete(SurveySection vo) {
		// TODO Auto-generated method stub
		session.delete("SurveySection.delete", vo);
	}

	@Override
	public SurveySection get(SurveySection vo) {
		// TODO Auto-generated method stub
		return session.selectOne("SurveySection.get", vo);
	}

	@Override
	public List<SurveySection> list(SurveySection vo) {
		// TODO Auto-generated method stub
		return session.selectList("SurveySection.getList",null);
	}

}
