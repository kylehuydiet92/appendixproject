package com.coeding.appendix.repository.impl;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.coeding.appendix.entity.Question;
import com.coeding.appendix.repository.QuestionRepository;
@Repository
public class QuestionRepositoryImpl implements QuestionRepository {
	@Autowired
	private SqlSessionTemplate session;
	@Override
	public void insert(Question vo) {
		// TODO Auto-generated method stub
		session.insert("Question.insert", vo);
	}

	@Override
	public void update(Question vo) {
		// TODO Auto-generated method stub
		session.update("Question.update",vo);
	}

	@Override
	public void delete(Question vo) {
		// TODO Auto-generated method stub
		session.delete("Question.delete", vo);
	}

	@Override
	public Question get(Question vo) {
		// TODO Auto-generated method stub
		return session.selectOne("Question.get", vo);
	}

	@Override
	public List<Question> list(Question vo) {
		// TODO Auto-generated method stub
		return session.selectList("Question.getList", vo);
	}

}
