package com.coeding.appendix.repository.impl;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.coeding.appendix.entity.Unit_Of_Measures;
import com.coeding.appendix.repository.MeasureRepository;
@Repository
public class MeasureRepositoryImpl implements MeasureRepository {
	@Autowired
	private SqlSessionTemplate session;



	@Override
	public void delete(Unit_Of_Measures vo) {
		session.update("Mearuse.delete",vo);
		
	}

	@Override
	public Unit_Of_Measures get(Unit_Of_Measures vo) {
		// TODO Auto-generated method stub
		return session.selectOne("Mearuse.get",vo);
	}

	@Override
	public List<Unit_Of_Measures> list(Unit_Of_Measures vo) {
		// TODO Auto-generated method stub
		return session.selectList("Mearuse.getList", null);
	}

	@Override
	public void insert(Unit_Of_Measures vo) {
		// TODO Auto-generated method stub
		session.insert("Mearuse.insert",vo);
	}

	@Override
	public void update(Unit_Of_Measures vo) {
		session.update("Mearuse.update",vo);
		
	}
	

}
