package com.coeding.appendix.repository.impl;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.coeding.appendix.entity.OptionGroup;
import com.coeding.appendix.repository.OptionGroupRepository;
@Repository
public class OptionGroupRepositoryImpl implements OptionGroupRepository {
	@Autowired
	private SqlSessionTemplate session;
	@Override
	public void insert(OptionGroup vo) {
		// TODO Auto-generated method stub
		session.insert("OptionGroup.insert",vo);
	}

	@Override
	public void update(OptionGroup vo) {
		// TODO Auto-generated method stub
		session.update("OptionGroup.update", vo);
	}

	@Override
	public void delete(OptionGroup vo) {
		// TODO Auto-generated method stub
		session.delete("OptionGroup.delete", vo);
	}

	@Override
	public OptionGroup get(OptionGroup vo) {
		// TODO Auto-generated method stub
		return session.selectOne("OptionGroup.get",vo);
	}

	@Override
	public List<OptionGroup> list(OptionGroup vo) {
		// TODO Auto-generated method stub
		return session.selectList("OptionGroup.getList", null);
	}

}
