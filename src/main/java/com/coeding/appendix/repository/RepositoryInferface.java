package com.coeding.appendix.repository;

import java.util.List;

public interface RepositoryInferface<T> {
	void insert(T vo);

	void update(T vo);

	void delete(T vo);

	T get(T vo);

	List<T> list(T vo);
}
