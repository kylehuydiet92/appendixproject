package com.coeding.appendix.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coeding.appendix.entity.Question;
import com.coeding.appendix.repository.QuestionRepository;
import com.coeding.appendix.service.QuestionService;
@Service
public class QuestionServiceImpl implements QuestionService {
	@Autowired
	private QuestionRepository dao;
	@Override
	public void insert(Question vo) {
		// TODO Auto-generated method stub
		if(dao !=null) {
			dao.insert(vo);
		}
	}

	@Override
	public void update(Question vo) {
		// TODO Auto-generated method stub
		if(dao !=null) {
			dao.update(vo);
		}
	}

	@Override
	public void delete(Question vo) {
		// TODO Auto-generated method stub
		if(dao !=null) {
			dao.delete(vo);
		}
	}

	@Override
	public Question get(Question vo) {
		if(dao !=null) {
			return dao.get(vo);
		}
		return null;
	}

	@Override
	public List<Question> list(Question vo) {
		// TODO Auto-generated method stub
		if(dao !=null) {
			return	dao.list(null);
		}
		return null;
	}

}
