package com.coeding.appendix.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coeding.appendix.entity.SurveySection;
import com.coeding.appendix.repository.SurveySectionRepository;
import com.coeding.appendix.service.SurveySectionService;
@Service
public class SurveySectionServiceImpl implements SurveySectionService {
	@Autowired
	private SurveySectionRepository dao;
	@Override
	public void insert(SurveySection vo) {
		// TODO Auto-generated method stub
		dao.insert(vo);
	}

	@Override
	public void update(SurveySection vo) {
		// TODO Auto-generated method stub
		dao.update(vo);
	}

	@Override
	public void delete(SurveySection vo) {
		// TODO Auto-generated method stub
		dao.delete(vo);
	}

	@Override
	public SurveySection get(SurveySection vo) {
		// TODO Auto-generated method stub
		return dao.get(vo);
	}

	@Override
	public List<SurveySection> list(SurveySection vo) {
		// TODO Auto-generated method stub
		return dao.list(null);
	}

}
