package com.coeding.appendix.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coeding.appendix.entity.Unit_Of_Measures;
import com.coeding.appendix.repository.MeasureRepository;
import com.coeding.appendix.service.MearuseService;
@Service
public class MearuseServiceImpl implements MearuseService {
	@Autowired
	private MeasureRepository dao;
	

	@Override
	public List<Unit_Of_Measures> list(Unit_Of_Measures vo) {
		// TODO Auto-generated method stub
		if(dao != null) {
			return dao.list(null);
		}
		return null;
	}




	@Override
	public void delete(Unit_Of_Measures vo) {
		// TODO Auto-generated method stub
		if(dao != null) {
		dao.delete(vo);
		}
	}


	@Override
	public Unit_Of_Measures get(Unit_Of_Measures vo) {
		if(dao != null) {
		return dao.get(vo);
		}
		return null;
	}




	@Override
	public void insert(Unit_Of_Measures vo) {
		// TODO Auto-generated method stub
		if(dao != null) {
		dao.insert(vo);
		}
	}




	@Override
	public void update(Unit_Of_Measures vo) {
		// TODO Auto-generated method stub
		if(dao != null) {
		dao.update(vo);
		}
	}

}
