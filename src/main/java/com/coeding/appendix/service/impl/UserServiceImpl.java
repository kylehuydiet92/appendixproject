package com.coeding.appendix.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coeding.appendix.entity.User;
import com.coeding.appendix.repository.UserRepository;
import com.coeding.appendix.service.UserService;
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository dao;
	@Override
	public void insert(User vo) {
		// TODO Auto-generated method stub
		dao.insert(vo);
	}

	@Override
	public void update(User vo) {
		// TODO Auto-generated method stub
		dao.update(vo);
	}

	@Override
	public void delete(User vo) {
		// TODO Auto-generated method stub
		dao.delete(vo);
	}

	@Override
	public User get(User vo) {
		// TODO Auto-generated method stub
		return dao.get(vo);
	}

	@Override
	public List<User> list(User vo) {
		// TODO Auto-generated method stub
		return dao.list(null);
	}

}
