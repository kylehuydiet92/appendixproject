package com.coeding.appendix.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coeding.appendix.entity.Organizations;
import com.coeding.appendix.repository.OrganizationRepository;
import com.coeding.appendix.service.OrganizationService;
@Service
public class OrganizationServiceImpl implements OrganizationService{
	@Autowired
	private OrganizationRepository dao;
	@Override
	public void insert(Organizations vo) {
		dao.insert(vo);
		
	}

	@Override
	public void update(Organizations vo) {
		dao.update(vo);
		
	}

	@Override
	public void delete(Organizations vo) {
		dao.delete(vo);
		
	}

	@Override
	public Organizations get(Organizations vo) {
		// TODO Auto-generated method stub
		return dao.get(vo);
	}

	@Override
	public List<Organizations> list(Organizations vo) {
		// TODO Auto-generated method stub
		return dao.list(null);
	}

}
