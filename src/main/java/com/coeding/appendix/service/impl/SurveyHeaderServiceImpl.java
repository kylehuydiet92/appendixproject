package com.coeding.appendix.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coeding.appendix.entity.Survey_Headers;
import com.coeding.appendix.repository.SurveyHeaderRepository;
import com.coeding.appendix.service.SurveyHeaderService;
@Service
public class SurveyHeaderServiceImpl implements SurveyHeaderService {
	@Autowired
	private SurveyHeaderRepository dao;
	@Override
	public void insert(Survey_Headers vo) {
		// TODO Auto-generated method stub
		dao.insert(vo);
	}

	@Override
	public void update(Survey_Headers vo) {
		// TODO Auto-generated method stub
		dao.update(vo);
	}

	@Override
	public void delete(Survey_Headers vo) {
		// TODO Auto-generated method stub
		dao.delete(vo);
	}

	@Override
	public Survey_Headers get(Survey_Headers vo) {
		// TODO Auto-generated method stub
		return dao.get(vo);
	}

	@Override
	public List<Survey_Headers> list(Survey_Headers vo) {
		// TODO Auto-generated method stub
		return dao.list(null);
	}

}
