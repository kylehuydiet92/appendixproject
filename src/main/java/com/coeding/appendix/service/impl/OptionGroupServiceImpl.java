package com.coeding.appendix.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coeding.appendix.entity.OptionGroup;
import com.coeding.appendix.repository.OptionGroupRepository;
import com.coeding.appendix.service.OptionGroupService;

@Service
public class OptionGroupServiceImpl implements OptionGroupService {
	@Autowired
	private OptionGroupRepository dao;

	@Override
	public void insert(OptionGroup vo) {
		// TODO Auto-generated method stub
		if (dao != null) {

			dao.insert(vo);
		}
	}

	@Override
	public void update(OptionGroup vo) {
		// TODO Auto-generated method stub
		if (dao != null) {

			dao.update(vo);
		}
	}

	@Override
	public void delete(OptionGroup vo) {
		// TODO Auto-generated method stub
		if (dao != null) {

			dao.delete(vo);
		}
	}

	@Override
	public OptionGroup get(OptionGroup vo) {
		// TODO Auto-generated method stub
		if (dao != null) {

			return dao.get(vo);
		}
		return null;

	}

	@Override
	public List<OptionGroup> list(OptionGroup vo) {
		// TODO Auto-generated method stub
		if (dao != null) {

			return dao.list(null);
		}
		return null;
	}

}
