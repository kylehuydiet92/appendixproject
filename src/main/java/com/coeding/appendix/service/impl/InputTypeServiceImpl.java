package com.coeding.appendix.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coeding.appendix.entity.Input_Types;
import com.coeding.appendix.repository.InputTypeRepository;
import com.coeding.appendix.service.InputTypeService;
@Service
public class InputTypeServiceImpl implements InputTypeService {
	@Autowired
	private InputTypeRepository dao;
	@Override
	public void insert(Input_Types vo) {
		if(dao != null) {
			dao.insert(vo);			
		}

	}

	@Override
	public void update(Input_Types vo) {
		// TODO Auto-generated method stub
		if(dao != null) {
			
			dao.update(vo);
		}
	}

	@Override
	public void delete(Input_Types vo) {
		// TODO Auto-generated method stub
		if(dao != null) {
			
			dao.delete(vo);
		}
	}

	@Override
	public Input_Types get(Input_Types vo) {
		// TODO Auto-generated method stub
		if(dao != null) {
			
			return dao.get(vo);
		}
		return null;
	}

	@Override
	public List<Input_Types> list(Input_Types vo) {
		// TODO Auto-generated method stub
		if(dao != null) {
			return dao.list(vo);
			
		}
		return null;
	}

}
