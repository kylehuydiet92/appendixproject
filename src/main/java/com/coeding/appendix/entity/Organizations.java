package com.coeding.appendix.entity;

public class Organizations {
	private int id;
	@Override
	public String toString() {
		return "Organizations [id=" + id + ", organizations_name=" + organizations_name + "]";
	}
	public Organizations(int id, String organizations_name) {
		super();
		this.id = id;
		this.organizations_name = organizations_name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOrganizations_name() {
		return organizations_name;
	}
	public void setOrganizations_name(String organizations_name) {
		this.organizations_name = organizations_name;
	}
	private String organizations_name;
	public Organizations() {
		// TODO Auto-generated constructor stub
	}
	
}
