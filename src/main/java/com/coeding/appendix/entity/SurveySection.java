package com.coeding.appendix.entity;

public class SurveySection {
	private int id;
	private String section_name;
	private String section_title;
	private String section_subheading;
	private boolean section_required_yn;
	private Survey_Headers survey_Headers;
	public SurveySection() {
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSection_name() {
		return section_name;
	}
	public void setSection_name(String section_name) {
		this.section_name = section_name;
	}
	public String getSection_title() {
		return section_title;
	}
	public void setSection_title(String section_title) {
		this.section_title = section_title;
	}
	public String getSection_subheading() {
		return section_subheading;
	}
	public void setSection_subheading(String section_subheading) {
		this.section_subheading = section_subheading;
	}
	public boolean isSection_required_yn() {
		return section_required_yn;
	}
	public void setSection_required_yn(boolean section_required_yn) {
		this.section_required_yn = section_required_yn;
	}
	public Survey_Headers getSurvey_Headers() {
		return survey_Headers;
	}
	public void setSurvey_Headers(Survey_Headers survey_Headers) {
		this.survey_Headers = survey_Headers;
	}
	public SurveySection(int id, String section_name, String section_title, String section_subheading,
			boolean section_required_yn, Survey_Headers survey_Headers) {
		super();
		this.id = id;
		this.section_name = section_name;
		this.section_title = section_title;
		this.section_subheading = section_subheading;
		this.section_required_yn = section_required_yn;
		this.survey_Headers = survey_Headers;
	}
	@Override
	public String toString() {
		return "SurveySection [id=" + id + ", section_name=" + section_name + ", section_title=" + section_title
				+ ", section_subheading=" + section_subheading + ", section_required_yn=" + section_required_yn
				+ ", survey_Headers=" + survey_Headers + "]";
	}



}
