package com.coeding.appendix.entity;

import java.util.Date;

public class User {
	public User() {
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPasswork_hashed() {
		return passwork_hashed;
	}
	public void setPasswork_hashed(String passwork_hashed) {
		this.passwork_hashed = passwork_hashed;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public Date getInvite_dt() {
		return invite_dt;
	}
	public void setInvite_dt(Date invite_dt) {
		this.invite_dt = invite_dt;
	}
	public Date getLast_login_dt() {
		return last_login_dt;
	}
	public void setLast_login_dt(Date last_login_dt) {
		this.last_login_dt = last_login_dt;
	}
	public int getInviter_user_id() {
		return inviter_user_id;
	}
	public void setInviter_user_id(int inviter_user_id) {
		this.inviter_user_id = inviter_user_id;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", passwork_hashed=" + passwork_hashed + ", email=" + email
				+ ", admin=" + admin + ", invite_dt=" + invite_dt + ", last_login_dt=" + last_login_dt
				+ ", inviter_user_id=" + inviter_user_id + "]";
	}
	public User(int id, String username, String passwork_hashed, String email, boolean admin, Date invite_dt,
			Date last_login_dt, int inviter_user_id) {
		super();
		this.id = id;
		this.username = username;
		this.passwork_hashed = passwork_hashed;
		this.email = email;
		this.admin = admin;
		this.invite_dt = invite_dt;
		this.last_login_dt = last_login_dt;
		this.inviter_user_id = inviter_user_id;
	}
	private int id;
	private String username;
	private String passwork_hashed;
	private String email;
	private boolean admin;
	private Date invite_dt;
	private Date last_login_dt;
	private int inviter_user_id;
}
