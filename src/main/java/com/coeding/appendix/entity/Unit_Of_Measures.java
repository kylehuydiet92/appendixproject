package com.coeding.appendix.entity;

public class Unit_Of_Measures {
	private int id;
	private String unit_of_measures_name;
	public Unit_Of_Measures() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Unit_Of_Measures [id=" + id + ", unit_of_measures_name=" + unit_of_measures_name + "]";
	}
	public Unit_Of_Measures(int id, String unit_of_measures_name) {
		super();
		this.id = id;
		this.unit_of_measures_name = unit_of_measures_name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUnit_of_measures_name() {
		return unit_of_measures_name;
	}
	public void setUnit_of_measures_name(String unit_of_measures_name) {
		this.unit_of_measures_name = unit_of_measures_name;
	}

}
