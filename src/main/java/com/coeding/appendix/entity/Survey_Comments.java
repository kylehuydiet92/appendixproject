package com.coeding.appendix.entity;

public class Survey_Comments {
	public Survey_Comments() {
		// TODO Auto-generated constructor stub
	}
	public Survey_Comments(int id, String comments, User user, Survey_Headers survey_headers) {
		super();
		this.id = id;
		this.comments = comments;
		this.user = user;
		this.survey_headers = survey_headers;
	}
	@Override
	public String toString() {
		return "Survey_Comments [id=" + id + ", comments=" + comments + ", users=" + user + ", survey_headers="
				+ survey_headers + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public User getUsers() {
		return user;
	}
	public void setUsers(User users) {
		this.user = users;
	}
	public Survey_Headers getSurvey_headers() {
		return survey_headers;
	}
	public void setSurvey_headers(Survey_Headers survey_headers) {
		this.survey_headers = survey_headers;
	}
	private int id;
	private String comments;
	private User user;
	private Survey_Headers survey_headers;
	
}
