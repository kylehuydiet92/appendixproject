package com.coeding.appendix.entity;

public class OptionGroup {
	public OptionGroup() {
		// TODO Auto-generated constructor stub
	}
	public OptionGroup(int id, String option_group_name) {
		super();
		this.id = id;
		this.option_group_name = option_group_name;
	}
	@Override
	public String toString() {
		return "OptionGroup [id=" + id + ", option_group_name=" + option_group_name + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOption_group_name() {
		return option_group_name;
	}
	public void setOption_group_name(String option_group_name) {
		this.option_group_name = option_group_name;
	}
	private int id;
	private String option_group_name;
}
