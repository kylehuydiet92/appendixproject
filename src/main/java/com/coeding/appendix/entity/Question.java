package com.coeding.appendix.entity;

public class Question {
	private int id;
	private int parent_id;
	private String question_name;
	private String question_subtext;
	private boolean question_required_yn;
	private boolean answer_required_yn;
	private boolean allow_mutiple_option_answers_yn;
	private int dependent_question_id;
	private int dependent_question_option_id;
	private int dependent_answer_id;
	private Input_Types inputType;
	private SurveySection surveySection;
	private OptionGroup optionGroup;
	public Question() {
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getParent_id() {
		return parent_id;
	}
	public void setParent_id(int parent_id) {
		this.parent_id = parent_id;
	}
	public String getQuestion_name() {
		return question_name;
	}
	public void setQuestion_name(String question_name) {
		this.question_name = question_name;
	}
	public String getQuestion_subtext() {
		return question_subtext;
	}
	public void setQuestion_subtext(String question_subtext) {
		this.question_subtext = question_subtext;
	}
	public boolean isQuestion_required_yn() {
		return question_required_yn;
	}
	public void setQuestion_required_yn(boolean question_required_yn) {
		this.question_required_yn = question_required_yn;
	}
	public boolean isAnswer_required_yn() {
		return answer_required_yn;
	}
	public void setAnswer_required_yn(boolean answer_required_yn) {
		this.answer_required_yn = answer_required_yn;
	}
	public boolean isAllow_mutiple_option_answers_yn() {
		return allow_mutiple_option_answers_yn;
	}
	public void setAllow_mutiple_option_answers_yn(boolean allow_mutiple_option_answers_yn) {
		this.allow_mutiple_option_answers_yn = allow_mutiple_option_answers_yn;
	}
	public int getDependent_question_id() {
		return dependent_question_id;
	}
	public void setDependent_question_id(int dependent_question_id) {
		this.dependent_question_id = dependent_question_id;
	}
	public int getDependent_question_option_id() {
		return dependent_question_option_id;
	}
	public void setDependent_question_option_id(int dependent_question_option_id) {
		this.dependent_question_option_id = dependent_question_option_id;
	}
	public int getDependent_answer_id() {
		return dependent_answer_id;
	}
	public void setDependent_answer_id(int dependent_answer_id) {
		this.dependent_answer_id = dependent_answer_id;
	}
	public Input_Types getInputType() {
		return inputType;
	}
	public void setInputType(Input_Types inputType) {
		this.inputType = inputType;
	}
	public SurveySection getSurveySection() {
		return surveySection;
	}
	public void setSurveySection(SurveySection surveySection) {
		this.surveySection = surveySection;
	}
	public OptionGroup getOptionGroup() {
		return optionGroup;
	}
	public void setOptionGroup(OptionGroup optionGroup) {
		this.optionGroup = optionGroup;
	}
	public Question(int id, int parent_id, String question_name, String question_subtext, boolean question_required_yn,
			boolean answer_required_yn, boolean allow_mutiple_option_answers_yn, int dependent_question_id,
			int dependent_question_option_id, int dependent_answer_id, Input_Types inputType,
			SurveySection surveySection, OptionGroup optionGroup) {
		super();
		this.id = id;
		this.parent_id = parent_id;
		this.question_name = question_name;
		this.question_subtext = question_subtext;
		this.question_required_yn = question_required_yn;
		this.answer_required_yn = answer_required_yn;
		this.allow_mutiple_option_answers_yn = allow_mutiple_option_answers_yn;
		this.dependent_question_id = dependent_question_id;
		this.dependent_question_option_id = dependent_question_option_id;
		this.dependent_answer_id = dependent_answer_id;
		this.inputType = inputType;
		this.surveySection = surveySection;
		this.optionGroup = optionGroup;
	}
	@Override
	public String toString() {
		return "Question [id=" + id + ", parent_id=" + parent_id + ", question_name=" + question_name
				+ ", question_subtext=" + question_subtext + ", question_required_yn=" + question_required_yn
				+ ", answer_required_yn=" + answer_required_yn + ", allow_mutiple_option_answers_yn="
				+ allow_mutiple_option_answers_yn + ", dependent_question_id=" + dependent_question_id
				+ ", dependent_question_option_id=" + dependent_question_option_id + ", dependent_answer_id="
				+ dependent_answer_id + ", inputType=" + inputType + ", surveySection=" + surveySection
				+ ", optionGroup=" + optionGroup + "]";
	}
	
	

	
	
	
	
}
