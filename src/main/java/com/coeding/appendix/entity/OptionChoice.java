package com.coeding.appendix.entity;

public class OptionChoice {
	private int id;
	private String option_choice_name;
	private OptionGroup optionGroup;
	public OptionChoice() {
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOption_choice_name() {
		return option_choice_name;
	}
	public void setOption_choice_name(String option_choice_name) {
		this.option_choice_name = option_choice_name;
	}
	public OptionGroup getOptionGroup() {
		return optionGroup;
	}
	public void setOptionGroup(OptionGroup optionGroup) {
		this.optionGroup = optionGroup;
	}
	
	@Override
	public String toString() {
		return "OptionChoice [id=" + id + ", option_choice_name=" + option_choice_name + ", optionGroup=" + optionGroup
				+ "]";
	}
	public OptionChoice(int id, String option_choice_name, OptionGroup optionGroup) {
		super();
		this.id = id;
		this.option_choice_name = option_choice_name;
		this.optionGroup = optionGroup;
	}
	
}
