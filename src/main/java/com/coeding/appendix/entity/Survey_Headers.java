package com.coeding.appendix.entity;

public class Survey_Headers {
	private int id;
	private String survey_headers_name;
	private String instructions;
	private String orther_header_info;
	private Organizations organizations;
	public Survey_Headers() {
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSurvey_headers_name() {
		return survey_headers_name;
	}
	public void setSurvey_headers_name(String survey_headers_name) {
		this.survey_headers_name = survey_headers_name;
	}
	public String getInstructions() {
		return instructions;
	}
	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	public String getOrther_header_info() {
		return orther_header_info;
	}
	public void setOrther_header_info(String orther_header_info) {
		this.orther_header_info = orther_header_info;
	}
	public Organizations getOrganizations() {
		return organizations;
	}
	public void setOrganizations(Organizations organizations) {
		this.organizations = organizations;
	}
	public Survey_Headers(int id, String survey_headers_name, String instructions, String orther_header_info,
			Organizations organizations) {
		super();
		this.id = id;
		this.survey_headers_name = survey_headers_name;
		this.instructions = instructions;
		this.orther_header_info = orther_header_info;
		this.organizations = organizations;
	}
	@Override
	public String toString() {
		return "Survey_Headers [id=" + id + ", survey_headers_name=" + survey_headers_name + ", instructions="
				+ instructions + ", orther_header_info=" + orther_header_info + ", organizations=" + organizations
				+ "]";
	}
	
}
