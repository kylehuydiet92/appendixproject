package com.coeding.appendix.entity;

public class Input_Types {
	private int id;
	private String input_types_name;
	public Input_Types() {
		// TODO Auto-generated constructor stub
	}
	public Input_Types(int id, String input_types_name) {
		super();
		this.id = id;
		this.input_types_name = input_types_name;
	}
	@Override
	public String toString() {
		return "Input_Types [id=" + id + ", input_types_name=" + input_types_name + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getInput_types_name() {
		return input_types_name;
	}
	public void setInput_types_name(String input_types_name) {
		this.input_types_name = input_types_name;
	}

}
